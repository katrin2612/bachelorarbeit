library(dplyr)
library(stringr)
library(grid)
library(igraph)
library(rlist)
library(readr)

# Sequenzen einlesen, Groessen definieren 
#FilesA <- unlist(strsplit("GAGCCA", split = ""))   
#FilesB <- unlist(strsplit("GACGGA", split = ""))

grossesBeispiel = c()
csv <- read.csv2(file="/Users/katrin/Desktop/csvbeispiel.csv")
#start <- Sys.time()
unterschiedetotalzeit = 0
for( i in 1 : length(csv[["Description_old"]])){
 FilesA <- csv[i,1]
 FilesB <- csv[i,2]

 FilesA <- unlist(strsplit(FilesA, split = " ", perl = TRUE))
 FilesB <- unlist(strsplit(FilesB, split = " ", perl = TRUE))
 FilesA <- FilesA[which(!FilesA %in% "")]
 FilesB <- FilesB[which(!FilesB %in% "")]

 N = length(FilesA)
 M = length(FilesB)
 O=N+1
 P=M+1

 #Edit Graph Raster mit Groesse 0 *P erstellen, Knoten gelabeled mit den Koordinaten
 g =  graph.lattice(c(O,P))
 lay <- layout.grid(g)
 V(g)$x <- rep(0:N, P)
 y_tmp <- rep(c("0"), O)
 for(i in 1:M){
  y_tmp <- c(y_tmp, rep(i, O))
 }
 V(g)$y <- as.numeric(y_tmp)
 V(g)$label <- paste(V(g)$x, V(g)$y, sep=",")
 xwert <- V(g)$x
 ywert <- V(g)$y

 # Platzhalter definieren und Koordinaten in Knoten durch Buchstaben aus Sequenzen an jeweiligen 
 # Positionen ersetzen
 xwert <- rep(c("_0_", FilesA), P)
 ywert <- rep(c("_0_"), O)
 for(i in 1:M){
  ywert <- c(ywert, rep(FilesB[i], O))
 }
 knoten <- rbind(xwert, ywert)
 V(g)$xwert <- xwert
 V(g)$ywert <- ywert
 V(g)$label <- paste(xwert, ywert, sep=",")

 #Matchpoints erkennen und diagonale Kanten dorthin einfuegen
 #start3 <- Sys.time()
 tabelle <- tibble(index= 1:length(xwert),x= xwert, y=ywert) %>% mutate(diagonal= ifelse(x==y,index,FALSE)) %>%
 filter(diagonal>1) %>% 
 select(diagonal) %>%
 mutate(from = diagonal-O-1) %>%
 mutate(to = diagonal)
 diagonals= c()
 j=1
 if (nrow(tabelle)>0){
  for (i in 1: nrow(tabelle)) {
    diagonals[j]= unlist(tabelle[i,"from"])
    diagonals[j+1]= unlist(tabelle[i,"to"])
    j=j+2
    }
  }
 #end3<- (Sys.time()-start3)
 g <- add_edges(g,diagonals)
 #plot(g)

 #kürzesten Weg durch den Graph finden, die Knoten dieses Pfades, welche durch
 #eine Diagonale erreicht wurden, speichern (pathdiagonals) und zugehörige Zeichen aus Sequenz als LCS(y)
 #ausgeben. Da bei Matches die Zeichen gleich sind wird hier nur der xwert genutzt. 
 asp <- shortest_paths(g, from =1 , to = (O*P), mode = c("out", "all","in"), weights = NULL)
 #asp <- all_shortest_paths(g, from =1 , to = (O*P), mode = c("out", "all","in"), weights = NULL) alternative fuer ALLE kuerzesten wege
 y <- NULL
 #for(i in 1: length(asp$vpath)){ #fuer alle kuerzesten pfade die es gibt
  i=1
  path <- asp$vpath[[i]]
  pathdiagonals <- NULL 
  for(j in 2: length(asp$vpath[[i]])){
   if(path[j]-path[j-1] == (O+1)) { 
     pathdiagonals <- c(pathdiagonals, path[j])
   }
  }
  y[i] <- list((V(g)[pathdiagonals]$xwert))
 #}
 y<- unique(y)
 print("longest common subsequence(s) is/are " )
 print(y)

 #Textunterschiede erkennen indem geprueft wird, ob ein Knoten durch
 #eine Diagonale erreicht wurde, eine Horizontale( dann wurde das Zeichen 
 #der Sequenz B hinzugefügt) oder Vertikale ( dann wurde es aus der ersten Sequenz 
 #entfernt). Die entsprechenden Vorzeichen werden hinzugefuegt.
 pathfinal <- asp$vpath[[1]]
 resultGraph = NULL
 #start5 <- Sys.time()
 for(j in (2 : length(pathfinal))){
    if(pathfinal[j]-pathfinal[j-1] == (O+1)) { #diagonale 
      resultGraph <- c(resultGraph, pathfinal[j]$xwert)
    }else if (pathfinal[j]-pathfinal[j-1] == O){#horizontale
      resultGraph <- c(resultGraph, "+") 
      resultGraph <- c(resultGraph, pathfinal[j]$ywert) #achtung: ywert statt xwert nutzen
    }else if (pathfinal[j]-pathfinal[j-1] == (1)){ #vertikale
      resultGraph <- c(resultGraph,"-") 
      resultGraph <- c(resultGraph,pathfinal[j]$xwert)
    }
 }
 #end5 <- Sys.time()-start5
 
 #Ausgabe 
 resultGraph <- paste(resultGraph, collapse = " ")
 grossesBeispiel = list.append(grossesBeispiel, "\n")
 grossesBeispiel = list.append(grossesBeispiel, "\n")
 grossesBeispiel = list.append(grossesBeispiel, resultGraph) 
 #unterschiedetotalzeit = unterschiedetotalzeit+end5
}
#cat(resultGraph,file="Graph_Zeichen.txt")
cat(grossesBeispiel,file="graph.txt")
print(grossesBeispiel)
#print(Sys.time() - start)
#print(end3)
#print(end5)

